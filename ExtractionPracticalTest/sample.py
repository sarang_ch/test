import numpy as np
from docx import Document
import glob

file_names = glob.glob("../data/*.docx") # gets all the docx files in the desired directory
word_list = np.array(["by and between", "by and among"]) # keyords that represent the sides of the contract

# this function finds the content of the paragraph that contains the keyword defined earlier
# it gets the paragraph after "supply agreement" title as the main paragraph
def main_paragraph(file_name):
    document = Document(file_name)

    header_found = False
    for i in np.arange(0, len(document.paragraphs)):
        paragraph = document.paragraphs[i].text
        if (not header_found) and ("supply agreement" in paragraph.lower()):
            header_found = True
        elif (header_found) and len(paragraph) > 10:
            return paragraph
    return ""

# this function gets the substring between two patterns in a string
def get_substring(string, pattern_start, pattern_end):
    pos_start = 0
    pos_end = len(string)

    if (pattern_start != "") and (string.find(pattern_start) != -1):
        pos_start = string.find(pattern_start) + len(pattern_start)

    if (pattern_end != "") and (string[pos_start:len(string)].find(pattern_end) != -1):
        pos_end = string[pos_start:len(string)].find(pattern_end) + pos_start

    substring = string[pos_start:pos_end]
    return substring    

# this function returns the index of the keyword that exists in the main paragraph
def which_keyword(string, list_of_words):
    cntr = 0
    for key_word in list_of_words:
        if key_word in string:
            return cntr
        else:
            cntr = cntr + 1
    return -1

# the pattern of the names after the keywords are studied 
# in this part the pattern is applied on the content of the main paragraph 
for file_name in file_names:
    paragraph = main_paragraph(file_name)
    if paragraph != "":
        word_list_index = which_keyword(paragraph, word_list)
        if word_list_index != -1:
            paragraph_part = get_substring(paragraph, word_list[word_list_index], "")
            name1 = get_substring(paragraph_part, "", ",")
            paragraph_part = get_substring(paragraph_part, name1 + ",", "")
            name2 = get_substring(paragraph_part, "and", ",")

            print("Contract file: " + file_name + "    First party: " + name1 + "    Second party: " + name2)


