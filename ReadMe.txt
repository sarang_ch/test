1) The "essay.docx" contains the essay.
2) The "ExtractionPracticalTest" directory contains a Python code "smple.py" that points to the folder "data" containing the word documents. By executing this code, each file name and their corresponding employer and employee name will be printed.
3) The "CodingTest" directory contains a JavaScript (Node.js) code "concept-net.js". By executing it on the terminal together with the word, a JSON string will be returned. 
	- example:  > node concept-net.js "football"
	- Note: I have limited number of records in the code to first value larger than 5 for faster performance. 
	
All these codes, files and assessments can also be checked out from a BitBucket repository with the following URL

https://sarang_ch@bitbucket.org/sarang_ch/test.git