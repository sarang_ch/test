/*
    the input_word is the word that is going to be called from the ConceptNet API
    the overallJsonStr keeps the overall json string
*/

function callApi(input_word, overallJsonStr)
{
    var myJsonStr = overallJsonStr.substring(1, overallJsonStr.length - 1);

    url = 'http://api.conceptnet.io/c/en/';
    limitter = '?offset=0&limit=100'; // limits the api response size

    var formatted_word = input_word; 
    formatted_word = input_word.replace(' ', '_'); // removes space in the input words 

    // it will continue till there is no next page information in the returned response
    while (limitter != undefined)
    {
        fullUrl = url + formatted_word + limitter;

        var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
        function httpGet(theUrl)
        {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", theUrl, false ); 
            xmlHttp.send( null );
            return xmlHttp.responseText;
        }

        var jsonStr = httpGet(fullUrl);
        var obj = JSON.parse(jsonStr);
        
        if(myJsonStr != '')
        {
            myJsonStr = myJsonStr + ', ';
        }

        myJsonStr = myJsonStr + refine(jsonStr, input_word, overallJsonStr);

        if (obj.hasOwnProperty("nextPage"))
        {
            limitter = obj.view.nextPage;
        }
        else
        {
            limitter = undefined;
        }
    }

    if (myJsonStr.slice(-2) == ', ')
    {
        myJsonStr = myJsonStr.substring(0, myJsonStr.length - 2);
    }
    return '{' + myJsonStr + '}';
}

/*
    the returned json string together with the searched word and the overall json string
    are fed to this function to find the IsA concept in them
*/
function refine(jsonStr, word, overallJsonStr) 
{
    var myJsonStr = '';
    var objOverall = JSON.parse(overallJsonStr);
    var obj = JSON.parse(jsonStr);
    obj.edges.forEach(element => {
        // takes records with IsA concept into account
        if (element.rel.label == "IsA")
        {
            // make sure the returned words are in English
            if ((element.start.language == "en") && (element.end.language == "en"))
            {
                // make sure that the child node contains the word
                if (element.start.label == word)
                {
                    if(myJsonStr != '')
                    {
                        myJsonStr = myJsonStr + ', ';
                    }

                    if (!objOverall.hasOwnProperty(word))
                    {
                        myJsonStr = myJsonStr + '\"' + element.end.label.replace(' ', '_') + '\"' +':{\"' + element.start.label.replace(' ', '_') + '\":{}}';
                    }
                    else
                    {
                        myJsonStr = myJsonStr + '\"' + element.end.label.replace(' ', '_') + '\"' +':{\"' + element.start.label.replace(' ', '_') + '\":' + JSON.stringify(objOverall[word]) + '}';
                    }
                }
            }
        }
    });

    return myJsonStr;
}


/*
    This functions is created to limit the number of returned nodes in the overall json
*/
function getNumberOfKeys(jsonObjStr)
{
    var jsonObj = JSON.parse(jsonObjStr);
    var cnt = 0;
    for (var key in jsonObj)
    {
        cnt = cnt + 1;
    }
    return cnt;
}

// var input_word = "field mouse"
var input_word = process.argv[2]; // gets the word from console argument

var finalJsonStr = '{}';
finalJsonStr = callApi(input_word, finalJsonStr);

// limit the number of nodes to about 5
while(getNumberOfKeys(finalJsonStr) < 5){
    objFinal = JSON.parse(finalJsonStr);
    for(var key in objFinal) {
        if(objFinal.hasOwnProperty(key)) {
            finalJsonStr = callApi(key, finalJsonStr);
        }
    }
}

console.log(finalJsonStr);